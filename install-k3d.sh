#!/bin/bash

#save as install-k3d.sh
#az vm create -g k3s --image UbuntuLTS --admin-username ubuntu --ssh-key-values ~/.ssh/id_rsa.pub -n k3s --custom-data install-k3d.sh
#az network nsg rule create -g k3x --nsg-name k3xNSG --priority 1002 --access Allow --protocol Tcp --destination-port-ranges 443 -n https
#az network nsg rule create -g k3x --nsg-name k3xNSG --priority 1001 --access Allow --protocol Tcp --destination-port-ranges 80 -n http

curl https://get.docker.com/ | sh
sudo usermod -aG docker ubuntu

curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash

k3d cluster create k3d -p "80:80@loadbalancer" -p "443:443@loadbalancer" --agents 2

su - ubuntu -c 'k3d kubeconfig merge k3d --switch-context -o /home/ubuntu/.kube/config'

wget "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" -O /usr/local/bin/kubectl
chmod +x /usr/local/bin/kubectl